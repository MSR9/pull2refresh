//
//  ViewController.swift
//  Pull2Refresh
//
//  Created by EPITADMBP04 on 4/7/20.
//  Copyright © 2020 PrahladReddy. All rights reserved.
//

import UIKit

class ViewController: UITableViewController {
    
    lazy var refresher: UIRefreshControl = {
    let refreshControl = UIRefreshControl()
        refreshControl.tintColor = .black
        refreshControl.addTarget(self, action: #selector(requestData), for: .valueChanged)
        return refreshControl
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if #available(iOS 10.0, *) {
        tableView.refreshControl = refresher
    } else {
    tableView.addSubview(refresher)
    }
  }

@objc
func requestData() {
    print("Requesting Data")
    
    let deadLine = DispatchTime.now() + .milliseconds(700)
    DispatchQueue.main.asyncAfter(deadline: deadLine) {
        self.refresher.endRefreshing()
    }
}
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 30+1
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.textLabel?.text = String(indexPath.row)
        return cell
    }
}
